initialization
initialize
	super initialize.
	inPlaceEditor := InPlacePropertyEditorComponent on: #text of: self.
	text := 'este es el texto original'.
	inPlaceParagraphEditor := InPlacePropertyEditorComponent on: #paragraph of: self.
	paragraph := 'Esto es un texto\ con espacios y newlines' withCRs.
	inPlaceParagraphEditor beTextArea