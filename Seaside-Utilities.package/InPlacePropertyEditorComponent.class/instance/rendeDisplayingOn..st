rendering
rendeDisplayingOn: html
	(headingLevel < 5
		ifTrue: [ html heading
				level: headingLevel;
				yourself ]
		ifFalse: [ html label ])
		with: [ html text: label.
			html space.
			html anchor
				callback: [ editing := true ];
				with: [ html tbsGlyphIcon iconPencil ] ].
	(textArea
		ifFalse: [ html paragraph ]
		ifTrue: [ html preformatted ]) with: self valueFromSubject