rendering
renderLogoutButtonOn: html
	html anchor
		callback: [ self logout ];
		with: [ html tbsNavbarButton
				tbsPullRight;
				with: 'Log out' ]