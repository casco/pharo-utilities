rendering
renderErrorsWarningsAndInformationsOn: html
	html break.
	errorMessages notEmpty
		ifTrue: [ errorMessages
				do: [ :each | 
					html tbsAlert
						beDanger;
						with: [ html
								strong: 'Oh snap! ';
								break.
							html text: each ] ].
			errorMessages := OrderedCollection new ].
	html break.
	warningMessages notNil
		ifTrue: [ warningMessages
				do: [ :each | 
					html tbsAlert
						beWarning;
						with: [ html
								strong: 'Warning: ! ';
								break.
							html text: each ] ].
			warningMessages := OrderedCollection new ].
	html break.
	informationMessages notEmpty
		ifTrue: [ informationMessages
				do: [ :each | 
					html tbsAlert
						beInfo;
						with: [ html
								strong: 'Information! ';
								break.
							html text: each ] ].
			informationMessages := OrderedCollection new ]