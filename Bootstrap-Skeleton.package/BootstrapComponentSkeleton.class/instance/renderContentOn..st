rendering
renderContentOn: html
	self collectStats.
	self renderNavigationbarOn: html.
	html tbsContainer: [ self renderMainContentOn: html ].
	html tbsContainer: [ self renderErrorsWarningsAndInformationsOn: html ].
	html tbsContainer: [ self renderReleaseInfoOn: html ]