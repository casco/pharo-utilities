rendering
renderReleaseInfoOn: html
	| memNow sessionsNow |
	memNow := (Smalltalk vm memorySize / 1048576) truncated printString.
	sessionsNow := self application sessions size.
	html paragraph
		with: [ html
				small: [ html text: 'Released: ' , self releaseInfo.
					html
						text:
							' - ' , memNow , 'MB now, ' , (MaxMemory / 1048576) truncated printString , 'MB top - ' , sessionsNow printString , ' now, '
								, MaxSessions printString , ' top' ] ]