callbacks
grantAccessOnValidCode
	(self validCodes includes: accessCode)
		ifTrue: [ self show: self homeComponent ]
		ifFalse: [ self errorMessage: 'Invalid access code' ].
	accessCode := ''