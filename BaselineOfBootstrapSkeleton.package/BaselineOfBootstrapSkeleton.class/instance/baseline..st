baselines
baseline: spec
	<baseline>
	spec
		for: #common
		do: [ self
				seaside: spec;
				bootstrap: spec.
			spec package: 'Bootstrap-Skeleton' with: [ spec requires: #('Seaside3' 'Bootstrap') ] ]