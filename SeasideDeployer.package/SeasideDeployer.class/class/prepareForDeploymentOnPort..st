scripts
prepareForDeploymentOnPort: port
	| instance |
	releaseDate := Date today printString.
	mode := 'deployment'.
	instance := self new.
	instance seasideDeploymentPort: port.
	instance prepareForDeployment.
	referenceCommit := instance referenceCommit