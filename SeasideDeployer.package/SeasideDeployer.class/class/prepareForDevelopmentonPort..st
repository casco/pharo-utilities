scripts
prepareForDevelopmentOnPort: port
	| instance |
	releaseDate := Date today printString.
	mode := 'development'.
	instance := self new.
	instance seasideDevelopmentPort: port.
	instance prepareForDevelopment.
	referenceCommit := instance referenceCommit