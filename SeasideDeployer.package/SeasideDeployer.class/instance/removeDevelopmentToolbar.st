deployment
removeDevelopmentToolbar
	| configuration |
	configuration := (WADispatcher default handlerAt: self applicationPath) configuration.
	configuration takeValue: {} forAttribute: (configuration attributeAt: #rootDecorationClasses)