common
registerApplication
	app := WAAdmin register: self applicationClass asApplicationAt: self applicationPath.
	self requiredLibraries do: [ :lib | app addLibrary: lib ].
	self configurationClasses
		do: [ :confClass | app configuration addParent: confClass instance ]