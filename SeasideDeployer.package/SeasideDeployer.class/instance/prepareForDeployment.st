deployment
prepareForDeployment
	self
		registerApplication;
		removeDevelopmentToolbar;
		turnOffDebugging;
		unregisterDevelopmentApplications;
		startServer