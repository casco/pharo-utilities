rendering
renderContentOn: html
	editing
		ifTrue: [ self renderEditingOn: html ]
		ifFalse: [ self renderNormalOn: html ].
