rendering
renderEditingOn: html
	html
		tbsForm: [ html
				paragraph: [ html tbsButton
						callback: [ 	editing := false ];
						beExtraSmall;
						with: 'Cancel'.
					html space.
					html tbsButton
						callback: [ subject perform: (selector , ':') asSymbol with: modifiedCode.
							editing := false ];
						beExtraSmall;
						with: 'Save' ].
			html textArea
				rows: 10;
				columns: 120;
				on: #modifiedCode of: self;
				placeholder: 'your source code here' ]