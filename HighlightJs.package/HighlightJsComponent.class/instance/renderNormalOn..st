rendering
renderNormalOn: html
	| codeTag |
	html
		preformatted: [ codeTag := html code.
			language ifNotNil: [ codeTag class: language ].
			codeTag with: modifiedCode ]