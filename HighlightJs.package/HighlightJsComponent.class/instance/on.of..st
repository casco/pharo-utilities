initialization
on: aSymbol of: anObject
	subject := anObject.
	selector := aSymbol.
	modifiedCode := subject perform: selector