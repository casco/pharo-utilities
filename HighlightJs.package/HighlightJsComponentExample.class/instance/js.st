initialization
js
^ '[
    {
        "id": 9010915605938508,
        "agentName": "Programable",
        "active": true,
        "conversations": [],
        "chainId": 8005623435559400,
        "transactionNumber": 1,
        "is_external": false,
        "uri": "http://localhost:3000/agents/9010915605938509",
        "agentType": "ProgrammableAgent",
        "userStore": {
            "scripts": {
                "processAcceptProposal": "console.log(''AcceptProposal received and ignored'');",
                "processCallForProposal": "console.log(''CallForProposal received and ignored'');",
                "processFailure": "console.log(''Failure received and ignored'');",
                "processInformResult": "console.log(''InformResult received and ignored'');",
                "processPropose": "console.log(''Propose received and ignored'');",
                "processRefuse": "console.log(''Refuse received and ignored'');",
                "processInformDone": "console.log(''InformeDone received and ignored'');",
                "processRejectProposal": "console.log(''RejectProposal received and ignored'');"
            },
            "products": {
                "Hope": {
                    "count": 1,
                    "price": 1
                }
            }
        },
        "suppliers": {}
    },
    {
        "id": 8006130933530660,
        "agentName": "Empacadora",
        "active": true,
        "conversations": [],
        "chainId": 8005623435559399,
        "transactionNumber": 1,
        "is_external": false,
        "uri": "http://localhost:3000/agents/8006130933530660",
        "agentType": "Empacadora",
        "userStore": {
            "bom": {
                "Tomates": 5,
                "Envases": 1,
                "Zapallos": 3,
                "Zanahorias": 3
            },
            "products": {
                "Paquetes de verduras": {
                    "count": 0,
                    "price": 30
                }
            }
        },
        "suppliers": {
            "Tomates": [
                "http://localhost:3000/agents/8010915605938504",
                "http://localhost:3000/agents/8005624233099432"
            ],
            "Envases": [
                "http://localhost:3000/agents/8010915605938504"
            ],
            "Zapallos": [
                "http://localhost:3000/agents/8010915605938504",
                "http://localhost:3000/agents/8005624233099432"
            ],
            "Zanahorias": [
                "http://localhost:3000/agents/8005624233099432"
            ]
        }
    },
    {
        "id": 8005624233099432,
        "agentName": "Granja 1",
        "active": true,
        "conversations": [],
        "chainId": 8005623435559399,
        "transactionNumber": 1,
        "is_external": false,
        "uri": "http://localhost:3000/agents/8005624233099432",
        "agentType": "Granja",
        "userStore": {
            "products": {
                "Tomates": {
                    "count": 2000,
                    "price": 10
                },
                "Zanahorias": {
                    "count": 1000,
                    "price": 1
                },
                "Zapallos": {
                    "count": 3000,
                    "price": 5
                }
            }
        },
        "suppliers": {}
    },
    {
        "id": 8010915605938504,
        "agentName": "Granja 2",
        "active": true,
        "conversations": [],
        "chainId": 8005623435559399,
        "transactionNumber": 1,
        "is_external": false,
        "uri": "http://localhost:3000/agents/8010915605938504",
        "agentType": "Granja",
        "userStore": {
            "products": {
                "Tomates": {
                    "count": 9000,
                    "price": 2
                },
                "Envases": {
                    "count": 5555,
                    "price": 20
                },
                "Zapallos": {
                    "count": 9000,
                    "price": 5
                }
            }
        },
        "suppliers": {}
    },
    {
        "id": 8016344967610717,
        "agentName": "Distribuidor de paquetes de verduras",
        "active": true,
        "conversations": [],
        "chainId": 8005623435559399,
        "transactionNumber": 1,
        "is_external": false,
        "uri": "http://localhost:3000/agents/8016344967610717",
        "agentType": "Distribuidor",
        "userStore": {
            "products": {
                "Paquetes de verduras": {
                    "count": 0,
                    "price": 100
                }
            }
        },
        "suppliers": {
            "Paquetes de verduras": [
                "http://localhost:3000/agents/8006130933530660"
            ]
        }
    }
]' 