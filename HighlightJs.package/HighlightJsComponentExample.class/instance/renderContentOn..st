rendering
renderContentOn: html
	html
		tbsContainer: [ html tbsPanel beDefault
				with: [ html tbsPanelHeading: [ html tbsPanelTitle: [ html text: 'Details of agent ' ] ].
					html
						tbsPanelBody: [ html anchor
								callback: [ subComponent editing: true ];
								with: [ html tbsGlyphIcon iconPencil ].
							html break.
							html render: subComponent ] ] ]