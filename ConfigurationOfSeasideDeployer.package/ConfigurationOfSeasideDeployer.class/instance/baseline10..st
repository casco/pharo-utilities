baselines
baseline10: spec
	<version: '1.0-baseline'>

	spec for: #'common' do: [
		spec blessing: #'baseline'.
		spec repository: 'bitbucket://casco/pharo-utilities'.
		spec 
			configuration: 'Bootstrap' with: [
				spec
					versionString: '0.17.1';
					loads: #();
					repository: 'http://smalltalkhub.com/mc/Pharo/MetaRepoForPharo60/main/Bootstrap' ];
			configuration: 'Seaside3' with: [
				spec
					versionString: '3.2.5';
					preLoadDoIt: nil;
					postLoadDoIt: nil;
					loads: #('default' 'REST' );
					repository: 'http://www.smalltalkhub.com/mc/Seaside/MetacelloConfigurations/main' ].
		spec package: 'SeasideDeployer' with: [
				spec requires: #('Bootstrap' 'Seaside3' ). ]. ].
